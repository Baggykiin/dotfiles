#!/usr/bin/env python3

# Configuration variables
net_device = "enp8s0"

dimensions = "1920x23"
underline_thickness = 2
# note the font ordering here: lemonbar will attempt to use the leftmost font
# whenever possible, falling back to a font further to the right when needed to
# draw unsupported characters. We abuse this principle to draw Powerline
# characters at a larger font size, which adds some padding to the bar.
# The misalignment resulting from differing font sizes is fixed by the -o
# argument after every font definition, which offsets their vertical alignment.
#fonts = "-f 'TeX Gyre Adventor:size=11:bold' -o -2 -f 'Inconsolata for Powerline:size=20' -o 1 -f 'FontAwesome:size=11' -o -5"
#fonts = "-f 'Futura LT:size=12:style=CondensedLight:demibold' -o -4 -f 'Inconsolata for Powerline:size=20' -o 1 -f 'FontAwesome:size=11' -o -5"
fonts = "-f 'Inconsolata:size=11:bold' -o -6 -f 'Inconsolata for Powerline:size=20' -o 0 -f 'FontAwesome:size=11'"

class Format:
    pass
colour = Format()
colour.white      = "FFFFFFFF"
colour.light      = "FFCCCCCC"
colour.gray       = "FFAAAAAA"
colour.dark       = "FF333333"
colour.black      = "FF000000"
colour.l_orange   = "FFCC8419"
colour.d_orange   = "FF9C6513"

colour.red        = "FFFF0000"

colour.reset      = "-"

align = Format()
align.left        = "%{l}"
align.centre      = "%{c}"
align.right       = "%{r}"

control = Format()
control.swap      = "%{R}"

attributes = Format()
attributes.under  = "u"
attributes.over   = "o"

triangle = Format()
triangle.right    = "\ue0b0"
triangle.left     = "\ue0b2"

chevron = Format()
chevron.right     = "\ue0b1"
chevron.left      = "\ue0b3"

# own classes
import resources
# standard imports
import threading
import os
import pexpect
import copy
from time import sleep


resources.net_device = net_device

# Start the resource monitor
thread = threading.Thread(target = resources.run)
thread.start()

# generate the argument list
args = "-u " + str(underline_thickness) + " -g " + dimensions + " " + fonts + " -p -B " + colour.dark + " -F " + colour.white

print("spawning lemonbar with:")
print("lemonbar " + args)

proc = pexpect.spawn("lemonbar " + args)

proc.setecho(False)

# Define some graphical characters
tr_left = "\ue0b0"
ch_left = "\ue0b1"
tr_right = "\ue0b2"
ch_right = "\ue0b3"

# Generate the correct colour codes for both foreground and background colour
def wrap(location, colour_code):
    return "%{" + location + "#" + colour_code + "}"

def wrap_add(attribute):
    return "%{+" + attribute + "}"

def wrap_rem(attribute):
    return "%{-" + attribute + "}"

names = [a for a in dir(colour) if not a.startswith('__')]
bcolour = copy.copy(colour)
ucolour = copy.copy(colour)
fcolour = copy.copy(colour)

[setattr(bcolour, name, (wrap("B", getattr(bcolour, name)))) for name in names]
[setattr(ucolour, name, (wrap("U", getattr(ucolour, name)))) for name in names]
[setattr(fcolour, name, (wrap("F", getattr(fcolour, name)))) for name in names]

attribute_names = [a for a in dir(attributes) if not a.startswith('__')]
add = copy.copy(attributes)
rem = copy.copy(attributes)

[setattr(add, attribute, (wrap_add(getattr(add, attribute)))) for attribute in attribute_names]
[setattr(rem, attribute, (wrap_rem(getattr(rem, attribute)))) for attribute in attribute_names]

# Takes a python format string and turns it into a lemonbar format string,
# stripping any newlines it encounters on the way.
def barformat(line, *args):
    return line.format(*args, add=add, rem=rem, f=fcolour, b=bcolour, u=ucolour, a=align, ctl=control, t=triangle, c=chevron).replace("\n", "")

while True:
    # prevent resources.py from updating any variables whilst we read them
    resources.lock.acquire()

    # any data formatting, if necessary, is done here

    numbers = {
            "I"    : 1,
            "II"   : 2,
            "III"  : 3,
            "IV"   : 4,
            "V"    : 5,
            "VI"   : 6,
            "VII"  : 7,
            "VIII" : 8,
            "IX"   : 9
            }

    desktopformats = {1: [], 2: [], 3: []}
    
    # build the desktop lists
    if 1 in resources.desktop_dict:
        for i in range(1, 1 + len(desktopformats)):
            desktops = resources.desktop_dict[i]
            uline_colour = ""
            fade_colour  = ""
            if i == 2:
                uline_colour = "{u.l_orange}"
                fade_colour  = "{f.gray}"
            else:
                uline_colour = "{u.l_orange}"
                fade_colour  = "{f.gray}"
            for desktop in desktops["desktops"]:
                if desktop != None:
                    name = desktop["name"]
                    form = uline_colour + name
                    if desktop["occupied"]:
                        form = "{f.white}" + form + fade_colour
                    if desktop["focused"]:
                        form = "{add.under}" + form + "{rem.under}"
                    form = fade_colour + form + fade_colour
                    desktopformats[i].append(form)
    desktop1 = " ".join(desktopformats[1])
    desktop2 = " ".join(desktopformats[2])
    desktop3 = " ".join(desktopformats[3])
    # quick fix for long windownames being generated by Chrome
    windowname = resources.windowname
    if windowname.endswith("Google Chrome"):
        windowname = "Google Chrome"

    # build the contents of the bar
    
    # prefix
    prefix = barformat("{f.white}{b.dark}")
    
    # left aligned
    window = barformat("{a.left}{}", windowname)
    
    # centre aligned
    desk   = barformat("{a.centre}{f.l_orange}{c.left}{f.white} ... {f.d_orange}{t.left}{b.d_orange}{f.white} {} {f.d_orange}{b.dark}{t.right}{f.white} ... {f.l_orange}{c.right}{f.white}", barformat(desktop1))
    #desk   = barformat("{a.centre}{f.l_orange}{c.left}{f.white} {} {f.d_orange}{t.left}{b.d_orange}{f.white} {} {f.d_orange}{b.dark}{t.right}{f.white} {} {f.l_orange}{c.right}{f.white}", barformat(desktop1), barformat(desktop2), barformat(desktop3))

    # right aligned
    right  = barformat("{a.right}")

    # TODO: confirm that these are calculated correctly.
    mem    = barformat("\uf080 {:.0f}/{:.1f} ", resources.mem_free/1024.0, resources.mem_total/1000.0/1024.0)
    cpu    = barformat("| \uf0ae {:4.1f}% ", resources.cpu)
    up     = barformat("\uf093 {:.2f}", resources.upload).rjust(7)
    dn     = barformat("\uf019 {:.2f}", resources.download).rjust(7)
    net    = barformat("{f.l_orange}{c.left}{f.white} {} | {} ", up, dn)
    volume = barformat("{f.l_orange}{c.left}{f.white} \uf027 {} ", resources.volume)
    date   = barformat("{f.l_orange}{t.left}{f.white}{b.l_orange} {} {} {} ", resources.time[0], resources.time[1], resources.time[2])
    time   = barformat("{f.d_orange}{t.left}{f.white}{b.d_orange} \uf017 {} ", resources.time[3])

    suffix = barformat("{b.dark}{f.white}")
    # join them all together
    line = prefix + window + desk + right + mem + cpu + net + volume + date + time + suffix
    # release the lock as we've finished reading variables
    resources.lock.release()
    proc.write(line + "\n")
    sleep(0.1)
