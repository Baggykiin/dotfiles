#!/usr/bin/env python3

import threading
import shlex
import os
import re
import pexpect
from time import sleep
from datetime import datetime
from threading import Lock
from subprocess import Popen, PIPE
# variable definitions

rbytes = 0
sbytes = 0

rbytes_o = 0
sbytes_o = 0

mem_free = 0
mem_total = 0

# configuration variables, should be set from run_lemonbar.py
net_device = ""


stop_requested = False

# output variables
upload = 0
download = 0
cpu = 0
time = []
volume = ""
windowname = ""
desktop_dict = {}

lock = Lock()


def run():
    threading.Thread(target = await_desktops).start()
    threading.Thread(target = await_cpu).start()
    step = 0.1
    index = 0
    # Some functions  may need to know  the amount of time  that has  passed
    # between their last  and current execution.  If we simply pass them the
    # amount of time  we've spent sleeping,  our numbers would be inaccurate
    # since we'd fail to account for the execution time of our update
    # functions. This is fixed by measuring the total execution time of all
    # update functions,  and then subtracting that amount from the amount of
    # time we're supposed to sleep, putting the total amount of time between
    # multiple executions at nearly the value of step,
    # with rougly 1ms of variance. This is accurate enough for our purposes.
    while not stop_requested:
        start = datetime.now()
        lock.acquire()
        if index % 10 == 0:
            update_dl(step * 10)
            update_datetime()
        update_volume_pamixer()
        update_memory()
        update_windowname()
        lock.release()
        index = index + 1
        end = datetime.now()
        elapsed = (end-start).microseconds / 1000000.0
        if elapsed < step:
            sleep(step - elapsed)


def update_datetime():
    global time
    # using strftime() and str.split together seems pointless, but it's
    # actually the easiest way to get locale-aware day and month names.
    current_datetime = datetime.now().strftime("%A|%e|%b|%H:%M:%S")
    time = str.split(current_datetime, "|")

# TODO: find a way to get the current volume without spawning a process every 100ms.
def update_volume_pamixer():
    global volume
    pamixer = Popen(["pamixer","--get-volume"], stdout=PIPE)
    output = pamixer.communicate()[0].decode("utf-8")
    volume = output + "%"

# def update_volume():
#    global volume
#    amixer = Popen(["amixer"], stdout=PIPE)
#    output = amixer.communicate()[0].decode("utf-8")
#    matches = re.search(r"Front Left: Playback .* \[(.*%)\]", output)
#    if matches is not None:
#        volume = matches.group(1)

def update_dl(elapsed):
    global rbytes, sbytes, rbytes_o, sbytes_o, upload, download, net_device 

    rbytes_o = rbytes
    sbytes_o = sbytes
    with open('/sys/class/net/' + net_device + '/statistics/rx_bytes', 'r') as rx_bytes:
        rbytes = int(rx_bytes.read())

    with open('/sys/class/net/' + net_device + '/statistics/tx_bytes', 'r') as tx_bytes:
        sbytes = int(tx_bytes.read())
    upload = sbytes - sbytes_o
    download = rbytes - rbytes_o

    upload = upload / 1024.0 / elapsed
    download = download / 1024.0 / elapsed

def update_memory():
    global mem_total
    global mem_free
    with open('/proc/meminfo', 'r') as meminfo:
        mem_values = {}
        for line in meminfo:
            matches = re.search(r"^(.*):\s+(\d+)\s*(kB)?\s*$", line)
            if matches is not None:
                mem_values[matches.group(1)] = int(matches.group(2))
        
        # TODO: check whether these calculations are correct 
        mem_total = mem_values["MemTotal"]
        mem_free = mem_total - mem_values["MemFree"] - mem_values["Buffers"] - mem_values["Cached"]

# TODO: find a way to get the current windowname without spawning a process every 100ms.
def update_windowname():
    global windowname
    xdotool = Popen(["xdotool", "getwindowfocus", "getwindowname"], stdout=PIPE)
    windowname = xdotool.communicate()[0].decode("utf-8").rstrip("\n")

def await_cpu():
    global cpu
    mpstat = pexpect.spawn("mpstat 1", timeout=999999)
    # skip a couple headers that come first
    mpstat.readline()
    mpstat.readline()
    mpstat.readline()
    while not stop_requested:
        output = mpstat.readline().decode("utf-8").rstrip("\r\n")
        if output.endswith("%idle"):
            continue
        if output == "":
            continue
        try:
            idle = re.search(r"^.* (.*)$", output).group(1)
            lock.acquire()
            cpu = 100 - float(idle)
            lock.release()
        except Exception as e:
            print("output: " + output)
    mpstat.close()

def mk_desktop():
    return {"focused": False, "desktops": [None] * 9}

def await_desktops():
    global desktop_dict
    bspc = pexpect.spawn("bspc control --subscribe", timeout=999999)

    numbers = {
        "I"    : 0,
        "II"   : 1,
        "III"  : 2,
        "IV"   : 3,
        "V"    : 4,
        "VI"   : 5,
        "VII"  : 6,
        "VIII" : 7,
        "IX"   : 8}


    while not stop_requested:
        desktops = bspc.readline()[1:-2].decode("utf-8")
        lock.acquire()
        desktop_dict = {1: mk_desktop(), 2:mk_desktop(), 3:mk_desktop()}
        elements = desktops.split(":")
        mon_id = 0
        for element in elements:
            key = element[:1]
            value = element[1:]
            focused = key.isupper()
            if key.lower() == "m":
                mon_id = int(value)
                desktop_dict[mon_id] = {"focused": focused, "desktops": [None] * 9}
            elif key.lower() == "o" or key.lower() == "u":
                num_val = numbers[value]
                desktop_dict[mon_id]["desktops"][num_val] = {"name": value, "focused": focused, "occupied": True}
            elif key.lower() == "f":
                num_val = numbers[value]
                desktop_dict[mon_id]["desktops"][num_val] = {"name": value, "focused": focused, "occupied": False}
            elif key == "L":
                desktop_dict[mon_id]["layout"] = value
        lock.release()
