# Baggykiin's dotfiles

Dotfiles for bspwm with sxhdk and lemonbar, vim, and Oh My Zsh. WIP.
The colour scheme especially is far from finalised.

![Preview Image](https://gitlab.com/Baggykiin/dotfiles/raw/master/example.png)

### Required software

In addition to the obvious ones:

#### For bspwm
 - wmname

#### For lemonbar

 - Python 3

#### Other

 - dmenu2
 - ?

### Installation

Clone the repository to the home directory where you want to install 
the dotfiles, then install the dotfiles for the program you want 
using GNU Stow.

```
git clone git@gitlab.com:Baggykiin/dotfiles.git
cd dotfiles
stow bspwm
```

Note that this symlinks your dotfiles to ~/dotfiles.
If you don't wish to do so, you can simply copy the relevant directory trees
directly to your home. Clone anywhere, then:

```
cd dotfiles
cp -r bspwm/. ~
```
Don't forget the `/.`, or it won't copy correctly.

Choose between either `zsh` or `oh-my-zsh` - don't install both. 
If you wish to use its config, install Oh My Zsh first.

### Additional themes

*Slack:* `#333333,#444444,#cc8419,#FFFFFF,#434343,#FFFFFF,#2ac947,#DB6668`

### Todo

- Sort out the colour scheme
- Change lemonbar input script to spawn fewer processes every second
