" Enable powerline
python from powerline.vim import setup as powerline_setup
python powerline_setup()
python del powerline_setup
set laststatus=2

" Try to autodetect the filetype. Also enable filetype plugins and indent
" files
filetype plugin indent on
" Enable syntax highlighting
syntax on
" Highlight all patterns that match your search
set hlsearch
" Start highlighting search options immediately
set incsearch

" Set tab width to 4 spaces
set tabstop=4
" Make tab insert indents instead of tabs at the beginning of a line
set smarttab
" Indent soft-wrapped lines
set breakindent
" Indent soft-wrapped lines 4 spaces beyond their original indent level
set showbreak=....

" Add line numbers
set number
" Highlight the current line
"set cursorline
" Highlight matching block characters ({[]})
set showmatch

" allow vertical movement across soft-wrapped lines
nnoremap j gj
nnoremap k gk

" Enable mouse interaction when available
if has('mouse')
	set mouse=a
endif

" Enable console menus, open with F4
source $VIMRUNTIME/menu.vim
set wildmenu
set cpo-=<
set wcm=<C-Z>
map <F4> :emenu <C-Z>

set backspace=indent,eol,start

