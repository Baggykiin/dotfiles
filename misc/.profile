export XDG_CONFIG_HOME="$HOME/.config"
# we prefer vim
export EDITOR=vim

# allow the user to use custom scripts
export PATH=$HOME/bin:${PATH}

